

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("index.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter out=response.getWriter();
	     response.setContentType("text/html");
	     
		String page=request.getParameter("page");
		
		if(page.equals("LoginPage")) {
			
			String username=request.getParameter("username");
			String password=request.getParameter("password");
			
			RegisterDao login=new RegisterDao();
			login.setEmail(username);
			login.setPassword(password);
			
			boolean areValid=Controller.Login(login);
			
			if(areValid) {
				response.sendRedirect("Home.jsp");
			}else {
				out.print("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">");
		        out.print("<h1 class='text-center jumbotron p-4 text-danger'>Sorry Username or Password Wrong</h1>");
		        RequestDispatcher rd=request.getRequestDispatcher("index.html");  
		        rd.include(request,response); 
			}
			
			
		}else if(page.equals("Registrationpage")) {
			
			String email=request.getParameter("email");
			String password=request.getParameter("password");
			String name=request.getParameter("name");
			String dob=request.getParameter("dob");
			String gender=request.getParameter("gender");
			String occupation=request.getParameter("occupation");
			String city=request.getParameter("city");
			String mobile=request.getParameter("mobile");
			
			RegisterDao reg= new RegisterDao(email,password,name,dob,gender,occupation,city,mobile);
			
			boolean areInserted=Controller.Registration(reg);
			
			if(areInserted) {
				response.sendRedirect("index.html");
				
			}else {
				out.print("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css\" integrity=\"sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk\" crossorigin=\"anonymous\">");
		        out.print("<h1 class='text-center jumbotron p-4 text-danger'>Sorry Went Wrong</h1>");
		        RequestDispatcher rd=request.getRequestDispatcher("Registration.jsp");  
		        rd.include(request,response); 
			}
		}
		
		
	}

}
