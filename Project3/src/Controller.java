
import java.sql.*;

public class Controller {

	public static boolean Registration(RegisterDao reg) {

		boolean areInserted=false;
		
		try {
			Connection con=Database.getConnection();
			PreparedStatement ps=con.prepareStatement("insert into library values (?,?,?,?,?,?,?,?)");
			ps.setString(1, reg.getEmail());
			ps.setString(2,reg.getPassword());
			ps.setString(3, reg.getName());
			ps.setString(4, reg.getDob());
			ps.setString(5, reg.getGender());
			ps.setString(6, reg.getOccupation());
			ps.setString(7, reg.getCity());
			ps.setString(8, reg.getMobile());

			ps.executeUpdate();
			
			ps.close();
			areInserted=true;
			con.close();
			
		}catch(Exception e) {
			areInserted=false;
			e.printStackTrace();
		}
		return areInserted;
		
	}
	
	

	public static boolean Login(RegisterDao login) {
		boolean areValid=false;
		try {
			
			Connection con=Database.getConnection();
			PreparedStatement ps=con.prepareStatement("select * from library where email =? and password =? ");
			ps.setString(1, login.getEmail());
			ps.setString(2, login.getPassword());
			
			ResultSet rs=ps.executeQuery();
			
		        areValid=rs.next();
		        
                 ps.close();
                 con.close();
		}catch(Exception e) {
			areValid=false;
			e.printStackTrace();
		}
		return areValid;
	}

	
}
