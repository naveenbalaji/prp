
public class RegisterDao {
	
	String email,password,name,dob,gender,occupation,city,mobile;
	
	public RegisterDao() {
		
	}
	public RegisterDao(String email, String password, String name, String dob, String gender, String occupation,
			String city, String mobile) {
		super();
		this.email = email;
		this.password = password;
		this.name = name;
		this.dob = dob;
		this.gender = gender;
		this.occupation = occupation;
		this.city = city;
		this.mobile = mobile;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	
}
