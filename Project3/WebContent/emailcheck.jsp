<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>

<%
if(request.getParameter("email_id")!=null) 
{
    String email=request.getParameter("email_id"); 
    String dburl="jdbc:mysql://localhost:3306/demoprojects"; 
    String dbusername="root"; 
    String dbpassword=""; 
    
    try
    {
        Class.forName("com.mysql.jdbc.Driver"); 
        Connection con=DriverManager.getConnection(dburl,dbusername,dbpassword); 
            
        PreparedStatement pstmt=null ; 
                
        pstmt=con.prepareStatement("SELECT * from library where email = ? "); 
        pstmt.setString(1,email);
        ResultSet rs=pstmt.executeQuery();  
        
        if(rs.next()){
        	%>
        	
        	<p class="text-danger">Email is Already Exist</p>
        	<%
        }else{
        	%>
            <p class="text-success">Email is Avaliable</p>
        <% 
        }

        con.close(); //close connection
    }
    catch(Exception e)
    {
        System.out.println(e);
    }
}
%>