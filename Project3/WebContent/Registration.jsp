<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
<title>Registration</title>
<!-- CSS only -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
 <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstraspcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<style>
    .main{
    margin-top: 10%;
    display:grid;
    place-items:center;
    background-color:#fffff;
    }
    .email{
    	width: 300px;
    }
    
    label {
  display: inline-block;
  width: 140px;
  text-align: right;
}​
</style>
 <script type="text/javascript">
        function email_Change()
        {
            var email = $("#inputEmail").val();
            $.ajax({
                type: "POST",
                url: "emailcheck.jsp",
                data: "email_id="+email,
                cache: false,
                success: function(response)
                {
                    $("#valid").html(response);
                }
            });
        }
     
        </script>
        <script>
        $(document).ready(function(){
        	  $("#inputEmail").blur(function(){
        		  var email = $("#inputEmail").val();
        		  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        		  if(!regex.test(email)) {
        			 document.getElementById("emailvaild").innerText="invalid Email Id";
        			  document.getElementById("emailvaild").style.color = "#ff0000";
        		  }else{
        			  document.getElementById("emailvaild").innerText="Valid Email Id";
        			  document.getElementById("emailvaild").style.color = "#008000";
        		  }
        	    
        	  });
        	});
        </script>
</head>
<body>
   <div class="container">
   	<div class="main">
   		  <div class="row">
   		  	<div class="col">
   		        <h1 class="text-center text-muted">Registration</h1>
   		              <form method="post" action="Main">
   		              
                              <div class="form-group">
                              <span><i class="text-center" id="emailvaild"></i></span>
                             <input type="email" id="inputEmail" class="form-control email" placeholder="E-Mail" onblur="email_Change()" name="email" required autofocus>
                             <span><i class="text-center" id="valid"></i></span>
                              </div>
                              
                             <input type="password" id="password" class="form-control" placeholder="Password" name="password" required>
                             

                             <input type="text" id="name" class="form-control" placeholder="Name" name="name" required>
                             
                              <input type="date" id="date" class="form-control" placeholder="Date of Birth" name="dob" required>
                              
                              <select class="form-control" name="gender">
                              <option value="-1">Gender</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Other">Other</option>
                              </select>
                               
                                <input type="text" id="occupation" class="form-control " placeholder="Occupation" name="occupation" required>
                                 <input type="text" id="city" class="form-control " placeholder="City" name="city" required>
                                  <input type="number" id="mobile" class="form-control " placeholder="Mobile" name="mobile" required>
                                  
                                 <input type="hidden" value="Registrationpage" name="page"/>
                                   
                             
   		                 <div class="form-row p-4">
   		               	       <div class="col">
   		   		                     <button class="btn btn-lg btn-success btn-block" type="submit">Register</button>
   		   	                 </div>
   		                </div>
   		                 <div class="form-row">
   		               	     <div class="col text-center">
   		               	     	<br>
   		               	     <h4><a href="index.html" class="text-info"  style="text-decoration : none"> &lt- Home</a></h4>
   		   	                 </div>
   		                  </div>
   		       
   		  		     </form>
   		  	</div>
   		  </div>
   	</div>
   </div>
</body>
</html>