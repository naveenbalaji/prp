import java.sql.*;
public class Controller {

	
	public static boolean areValidUser(LoginDao loginDao) {
		boolean areValid=false;
		try {
			
			Connection con=Database.getConnection();
			PreparedStatement ps=con.prepareStatement("select * from demoproject where username = ? and password = ?");
			ps.setString(1, loginDao.getUsername());
			ps.setString(2, loginDao.getPassword());
			
			ResultSet rs=ps.executeQuery();
			
		        areValid=rs.next();
		        
                 ps.close();
                 con.close();
		}catch(Exception e) {
			areValid=false;
			e.printStackTrace();
		}
		return areValid;
	}
	
	
	public static boolean passwordChange(String username, String oldpass,String newpass) {
		boolean arechanged=false;
		String tempPass="";
               try {
			Connection con=Database.getConnection();
			PreparedStatement ps=con.prepareStatement("select password from demoproject where username= ?");
			ps.setString(1,username);
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()) {
				tempPass=rs.getString("password");
			}
		           if(oldpass.equals(tempPass)) {
		        	   try {
				   		 PreparedStatement psmt=con.prepareStatement("update demoproject set password= ? where username = ? ");
				   		psmt.setString(1,newpass);
				   		psmt.setString(2, username);
				   		
				   		psmt.executeUpdate();
				   		arechanged=true;
				   		psmt.close();
		        		   
		        	   }catch(Exception e) {
		        		   arechanged=false;
		        		   e.printStackTrace();
		        	   }
		        	   
		           }else {
		        	   arechanged=false;
		           }
		        	   
                 ps.close();
                 con.close();
		}catch(Exception e) {
			arechanged=false;
			e.printStackTrace();
		}
		return arechanged;
	}
	
	
	public static boolean insertion(LoginDao registration) {
		boolean areInserted=false;
		
		try {
			Connection con=Database.getConnection();
			PreparedStatement ps=con.prepareStatement("insert into demoproject values(?,?)");
			ps.setString(1, registration.getUsername());
			ps.setString(2, registration.getPassword());
			ps.executeUpdate();
			
			areInserted=true;
			ps.close();
			con.close();
		}catch(Exception e) {
			areInserted=false;
		e.printStackTrace();	
		}
		return areInserted;
	}
	
	
}
