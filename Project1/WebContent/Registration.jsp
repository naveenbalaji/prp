<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
<title>Registration</title>
<!-- CSS only -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstraspcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<style>
    .main{
    margin-top: 20%;
    display:grid;
    place-items:center;
    background-color:#fffff;
    }
    .email ,.pass{
    	width: 300px;
    }
</style>
<script>

function validate(){
	var pass=document.getElementById("newPassword").value;
	var confirmpass=document.getElementById("confirmpassword").value;
	if(pass===confirmpass){
	}else{
		alert("Please enter matching new passord and Conform password");
		location='Registration.jsp';
	}
	
}
</script>
</head>
<body>
   <div class="container">
   	<div class="main">
   		  <div class="row">
   		  	<div class="col">
   		        <h1 class="text-center text-muted">Registration</h1>
   		              <form method="post" action="Main">
   		              
   		  	                 <label for="inputEmail" class="sr-only">Username</label>
                             <input type="email" id="inputEmail" class="form-control email" placeholder="Username" name="username" required autofocus>
                             
                             <label for="newpassword" class="sr-only">New Password</label>
                             <input type="password" id="newPassword" class="form-control pass" placeholder="New Password" name="password" required>
                             
                             <label for="confirmpassword" class="sr-only">Conform Password</label>
                             <input type="password" id="confirmpassword" class="form-control pass" placeholder="Conform Password" name="cpassword" required>
                             
                             <input type="hidden" value="RegistrationPage" name="page"/>
                             
   		                 <div class="form-row p-4">
   		               	     <div class="col">
   		   		         <button class="btn btn-lg btn-success btn-block" type="submit" onclick="validate()" >ADD</button>
   		   	                 </div>
   		                  </div>
   		                   <div class="form-row">
   		               	     <div class="col text-center">
   		               	     	<br>
   		               	     <h4><a href="index.html" class="text-info"  style="text-decoration : none"> &lt- Home</a></h4>
   		   	                 </div>
   		                  </div>
   		  		     </form>
   		  	</div>
   		  </div>
   	</div>
   </div>
</body>
</html>