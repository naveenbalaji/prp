package BID;

public class BidDao {
	
	String ItemID,ItemName,Name,email,amount;
	
	public BidDao() {	
	}

	public BidDao(String itemID, String itemName, String name, String email, String amount) {
		super();
		ItemID = itemID;
		ItemName = itemName;
		Name = name;
		this.email = email;
		this.amount = amount;
	}

	public String getItemID() {
		return ItemID;
	}

	public void setItemID(String itemID) {
		ItemID = itemID;
	}

	public String getItemName() {
		return ItemName;
	}

	public void setItemName(String itemName) {
		ItemName = itemName;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
